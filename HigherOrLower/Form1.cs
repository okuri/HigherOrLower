﻿/*
 * 
 *              Higher or Lower
 *        Developed by: SovietVallhund  
 * 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HigherOrLower
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Random number = new Random();
        public static int randNum = number.Next(1, 20);

        private void ansBox_TextChanged(object sender, EventArgs e)
        {
            ansBox.MaxLength = 2; // changes the max length of ansBox to 2 characters
            
            int value;
            if (int.TryParse(ansBox.Text, out value)) // sets the minimum number to 1 and max to 20
            {
                if (value > 20)
                    ansBox.Text = "20";
                else if (value < 1)
                    ansBox.Text = "1";
            }
        }

        int lives = 5;
        private void guessButton_Click(object sender, EventArgs e)
        {
            void gameStart() // creates the method gameStart(); will run when the user plays again
            {
                randNum = number.Next(1, 20);
                lives = 5;
                livesLbl.Text = "Lives: " + Convert.ToString(lives);
                ansLbl.ForeColor = Color.FromArgb(0, 0, 0);
                ansLbl.Left = 118;
                ansLbl.Text = "---";
                ansBox.Text = "";
            }

            void gameEnd() // creates the method gameEnd(); ran when you run out of lives or guess correctly
            {
                DialogResult result = MessageBox.Show("Would you like to play again?", "Game Over",
                                MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    gameStart();
                }
                else
                {
                    Application.Exit();
                }
            }
            try
            {
                int plyGuess = Int32.Parse(ansBox.Text); // parses ansBox.Text
                if (plyGuess > randNum) // pulls randNum from public class GlobalVariables
                {
                    ansLbl.Left = 90;
                    ansLbl.ForeColor = Color.FromArgb(255, 0, 0);
                    ansLbl.Text = "LOWER";
                    lives = lives - 1;
                    livesLbl.Text = "Lives: " + Convert.ToString(lives);
                    if (lives == 0)
                    {
                        gameEnd();
                    }
                }
                else if (plyGuess < randNum)
                {
                    ansLbl.Left = 88;
                    ansLbl.ForeColor = Color.FromArgb(255, 0, 0);
                    ansLbl.Text = "HIGHER";
                    lives = lives - 1;
                    livesLbl.Text = "Lives: " + Convert.ToString(lives);
                    if (lives == 0)
                    {
                        gameEnd();
                    }
                }
                else if (plyGuess == randNum)
                {
                    ansLbl.Left = 80;
                    ansLbl.ForeColor = Color.FromArgb(0, 200, 0);
                    ansLbl.Text = "CORRECT!";
                    gameEnd();
                }
            }
            catch
            {
                MessageBox.Show("Please input a value 1 - 20.", "ERROR",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}